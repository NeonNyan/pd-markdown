#!/usr/bin/env python3

#!/usr/bin/env python3
#
# pd-markdown.py
#

'''
Given a `ROMID` and `PDLANG` and the id argument, this script will generate a markdown file for the given id.
'''

import os
import sys
import json
from pathlib import Path
import markdown

input_id=sys.argv[1].upper()
output_file=Path(sys.argv[2])
input_lang=os.getenv('PDLANG', 'en') # NOTE: is this best practice?
input_romid=os.getenv('ROMID', 'ntsc-final')

ge_lv_id = input_id.split('_')[1].lower()
lv_nm = input_id.split('_')[2]
lang_data = None
with open(Path(os.getenv('PD')) / f"src/assets/{input_romid}/lang/{ge_lv_id}.json", "rt") as f:
    lang_data = json.load(f)

input_string = [l for l in lang_data if l['id'] == input_id][0][input_lang]

tokens = input_string.split('\n\n')
tokens[-1] = tokens[-1].replace('\n', '')

text_type = 'dialog'
if '|' in input_string and input_string.endswith('\n\nEND\n'):
    text_type = 'mission'
pd_md= "# " + input_id.replace('_','-').lower() + " - " + input_lang + " - " + input_romid + " {:" + f"#{input_id.replace('_','-').lower()} lang={input_lang} romid={input_romid}" + f" text-type={text_type}"+ "}"+ "\n"
for t in tokens:
    if "|" in t or t == "END":
        pd_md+="## " + t.replace("|", "")+"\n"
    else:
        pd_md+=t+"\n"


# we can't use print because it will add a newline
with open(output_file, "wt") as f:
    f.write(pd_md)
