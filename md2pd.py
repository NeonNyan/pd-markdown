#!/usr/bin/env python3

import os
import sys
import json
from pathlib import Path
import markdown
import lxml.html as lhtml

input_file = sys.argv[1]
html = None
header_str = None
md_str = None
header = None

with open(input_file, "rt") as f:
    md_str = f.read()
for m in md_str.split('\n'):
    if m.startswith('#') and header_str is None:
        header_str = markdown.markdown(m, extensions=['attr_list'])
        continue

if header_str is None:
    raise Exception("No header found in markdown file.")
header = lhtml.fromstring(markdown.markdown(header_str, extensions=['attr_list']))

lang = header.get("lang")
romid = header.get("romid")
text_type = header.get("text-type")
id = header.get("id")

if lang is None or romid is None or text_type is None or id is None:
    raise Exception("Missing required attributes from header.")

# Remove header from markdown
md_str = "\n".join(md_str.split("\n")[1:])

if text_type == "dialog":
    print(json.dumps(md_str))
elif text_type == "mission":
    with open("/dev/stdout", 'wt') as f:
        retval = ""
        retval = md_str.replace("\n## END","\nEND")
        retval = "|" + retval.replace("\n## ", "\n|")[3:].replace("\n", "\n\n")[:-1]
        f.write(json.dumps(retval))
else:
    raise Exception("Unknown text-type: " + text_type)

print(json.dumps({"lang": lang, "romid": romid, "text-type": text_type, "id": id}), file=sys.stderr)
